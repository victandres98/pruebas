$().ready(function () {
    $('#form').submit(function (e) {
        e.preventDefault();
        var input = $('#numbers').val().trim();
        var numbers = saveNumbers(input, ' ');
        var table = $('#table');
        drawTable(table, numbers)
        var n = parseInt($('#n').val());

        // PROMEDIOS
        var avg = averageP(numbers);
        var z0 = chi(avg, numbers);
        var dataPromedio = $('#data-promedios');
        dataPromedios(dataPromedio, avg, z0);

        // FRECUENCIAS
        var intervals = generateIntervals(numbers, n);
        var fe = fE(numbers, n);
        var fo = fO(intervals, n);
        var chi2 = chiCuadrado(fe, fo);
        var gl = gradeL(n);
        var tableFrecuence = $('#table-frecuencias');
        tableFrecuencias(tableFrecuence, fe, fo, n);
        var dataFrecuence = $('#data-frecuencias');
        data(dataFrecuence, gl, chi2);

        // SERIES
        var parejas = generateParejas(numbers);
        var celdas = generateCeldas(n, parejas);
        var feS = fESeries(numbers, n)
        var foS = fOSeries(celdas, n)
        var chi2S = chiCuadradoSeries(feS, foS, numbers, n);
        var glS = gradeLSeries(n);
        var tableSerie = $('#table-series');
        tableSeries(tableSerie, foS, n);
        var dataSeries = $('#data-series');
        data(dataSeries, glS, chi2S);

        //KOLMOGOROV
        var sortedNumbers = sortNumbers(numbers);
        var fn = distributionAcumulate(sortedNumbers, numbers.length);
        var Dn = stadisticKS(fn, numbers);
        var dn = getdn(numbers.length);
        var divKS = $('#data-ks');
        ks(Dn, dn, divKS);

        //POKER
        var array = numbersArray(numbers);
        var quintile = checkPoker0rQuintille(array, 5);

        var array1 = updateArray(array, quintile);
        var poker = checkPoker0rQuintille(array1, 4);

        var array2 = updateArray(array1, poker);
        var full = checkFull(array2);

        var array3 = updateArray(array2, full);
        var third = checkThird(array3);

        var array4 = updateArray(array3, third);
        var twoPairs = checkTwoPairs(array4);

        var array5 = updateArray(array4, twoPairs);
        var pair = checkPairs(array5);

        var array6 = updateArray(array5, pair);

        var FEPoker = getPokerFE(numbers.length);
        var FOPoker = getPokerFO(array6.length, pair.length, twoPairs.length, third.length,
            full.length, poker.length, quintile.length);
        var chiCuadradoPoker = chiPoker(FEPoker, FOPoker);
        var pokerDiv = $('#data-poker');
        var pokerTable = $('#table-poker');
        pokerData(pokerDiv, pokerTable, FEPoker, FOPoker, chiCuadradoPoker);

        //CORRIDAS
        var numbersBinary = convertToBinary(numbers);
        var randomDiv = $('#data-random-run');
        randomData(randomDiv, numbersBinary);
    });
});

// PRUEBA DE LOS PROMEDIOS
function averageP(numbers) {
    var sum = 0;
    for (var i = 0; i < numbers.length; i++) {
        sum += numbers[i];
    }
    var avg = sum / numbers.length;
    return avg;
}

function chi(x, N) {
    var z = 0;
    z = ((x - (1 / 2)) * Math.sqrt(N.length)) / Math.sqrt(1 / 12);
    return z;
}

function dataPromedios(div, avg, z0) {
    var html = `<p><b>Promedio Aritmético: </b><span>${redondear(avg, 2)}</span><br>`;
    html += `<b>Z<sub>0</sub>: </b><span>|${redondear(z0, 2)}|</span></p>`;
    div.html(html);
}


// PRUEBA DE FRECUENCIAS
function generateIntervals(numbers, n) {
    var intervals = {};
    for (let i = 1; i <= n; i++) {
        intervals[`interval${i}`] = [];
    }
    for (let j = 0; j < numbers.length; j++) {
        for (let k = 1; k <= n; k++) {
            if (numbers[j] < k / n) {
                intervals[`interval${k}`].push(numbers[j]);
                break;
            }

        }
    }
    return intervals;
}

function fE(numbers, n) {
    return numbers.length / n;
}

function fO(intervals, n) {
    var fo = [];
    for (let i = 0; i < n; i++) {
        fo.push(intervals[`interval${i + 1}`].length);
    }
    return fo;
}

function chiCuadrado(fe, fo) {
    var suma = 0;
    for (var i = 0; i < fo.length; i++) {
        suma += Math.pow(fo[i] - fe, 2);
    }
    var chi = suma / fe;
    return chi;
}

function gradeL(n) {
    return (n - 1) * (2 - 1);
}

function tableFrecuencias(table, fe, fo, n) {
    var thead = table.find('thead');
    var tbody = table.find('tbody');
    var htmlHead = `<tr><th>Frecuencia</th>`;
    var htmlBody = `<tr><td>Frecuencia Esperada</td>`;
    for (let i = 1; i <= n; i++) {
        htmlHead += `<th>${redondear(i / n, 2)}</th>`;
        htmlBody += `<td align="left">${redondear(fe, 2)}</td>`
    }
    htmlHead += `</tr>`;
    htmlBody += `</tr><tr><td>Frecuencia Observada</td>`;
    fo.forEach(element => {
        htmlBody += `<td align="left">${element}</td>`;
    });
    htmlBody += `</tr>`;
    thead.html(htmlHead);
    tbody.html(htmlBody);
}

function data(div, gl, chi2) {
    var html = `<p><b>Grado de Libertad (gl): </b><span>${gl}</span><br>`;
    html += `<b>Chi-cuadrado: </b><span>${redondear(chi2, 2)}</span></p>`;
    div.html(html);
}


// PRUEBA DE SERIES
function generateParejas(numbers) {
    var parejas = []
    for (let i = 1; i < numbers.length; i++) {
        parejas.push([numbers[i - 1], numbers[i]])
    }
    return parejas;
}

function generateCeldas(n, parejas) {
    var celdas = {};
    for (let i = 1; i <= n; i++) {
        for (let j = 1; j <= n; j++) {
            celdas[`celda-${i}-${j}`] = [];
        }
    }

    for (let i = 0; i < parejas.length; i++) {
        for (let j = 1; j <= n; j++) {
            if (parejas[i][0] < j / n) {
                for (let k = 1; k <= n; k++) {
                    if (parejas[i][1] < k / n) {
                        celdas[`celda-${j}-${k}`].push(parejas[i]);
                        break;
                    }
                }
                break;
            }
        }
    }
    return celdas;
}

function fESeries(numbers, n) {
    return (numbers.length - 1) / Math.pow(n, 2);
}

function fOSeries(celdas, n) {
    var fo = [];
    for (let i = 1; i <= n; i++) {
        for (let j = 1; j <= n; j++) {
            fo.push(celdas[`celda-${i}-${j}`].length);
        }
    }
    return fo;
}

function chiCuadradoSeries(fe, fo, numbers, n) {
    var suma = 0;
    for (var i = 0; i < fo.length; i++) {
        suma += Math.pow(fo[i] - fe, 2);
    }
    var chi = Math.pow(n, 2) / (numbers.length - 1);
    chi = chi * suma;
    return chi;
}

function gradeLSeries(n) {
    return Math.pow(n - 1, 2);
}

function tableSeries(table, fo, n) {
    var thead = table.find('thead');
    var tbody = table.find('tbody');
    var htmlHead = `<tr><th>y/x</th>`;
    var htmlBody = ``;
    for (let i = 0; i < n; i++) {
        htmlHead += `<th>${redondear((i + 1) / n, 2)}</th>`;
        htmlBody += `<tr><td><br>${redondear((i + 1) / n, 2)}</td>`;
        htmlBody += `<td align="left">${fo[i]}</td>`;
        var ind = i;
        for (let j = 0; j < n - 1; j++) {
            ind += n;
            htmlBody += `<td align="left">${fo[ind]}</td>`;
        }
        htmlBody += `</tr>`;
    }
    htmlHead += `</tr>`;
    thead.html(htmlHead);
    tbody.html(htmlBody);
}