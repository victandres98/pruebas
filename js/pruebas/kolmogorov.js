function sortNumbers(numbers) {
    return numbers.sort();

}

function distributionAcumulate(numbers, n) {
    var f = [];
    for (let i = 1; i <= numbers.length; i++) {
        var fn = i / n;
        f.push(fn);
    }
    return f;
}

function stadisticKS(fn, numbers) {
    var Dn = 0;
    var aux = [];
    for (let i = 0; i < fn.length; i++) {
        aux.push(Math.abs(fn[i] - numbers[i]));
    }
    Dn = Math.max(...aux);
    return redondear(Dn, 3);
}

function getdn(n) {
    if (d[n]) {
        return d[n];
    } else {
        var dn = 0;
        if (n < 100) {
            var aux = Object.keys(d);
            for (let i = 0; i < aux.length; i++) {
                if (n < aux[i]) {
                    dn = (d[aux[i]] + d[aux[i - 1]]) / 2;
                    break;
                }
            }
        } else {
            dn = 1.36 / Math.sqrt(n);
        } return redondear(dn, 3);
    }
}

function ks(Dn, dn, div) {
    var html = '';
    html += '<p><b>Dn: </b><span>' + Dn + '</span><br>';
    html += '<b>dn: </b><span>' + dn + '</span><br>';
    if (Dn <= dn) {
        html += '<b>Los números pseudoaleatorios provienen de una distribución uniforme</b></p>'
    } else {
        html += '<b>Los números pseudoaleatorios no provienen de una distribución uniforme</b></p>'
    }
    div.html(html);
}