$().ready(function () {
    $('#form').submit(function (e) {
        e.preventDefault();
        var input = $('#numbers').val().trim();
        var numbers = saveNumbers(input, ' ');
        var N = numbers.length;
        var table = $('#table');
        drawTable(table, numbers);
        $('#N').html(`En total: <b>${N}</b>`);
        var n = parseInt($('#n').val());

        //CHI CUADRADO
        var avgChi = average(numbers);
        console.log('avgChi', avgChi);
        var vrcChi = variance(numbers, avgChi);
        console.log('vrcChi', vrcChi);
        var intervalsChi = generateIntervals(numbers, n);
        console.log('intervalsChi', intervalsChi);
        var chart = $('#chi-chart');
        createHistogram(intervalsChi, chart);
        var chiFO = chiCuadradoFO(intervalsChi);
        console.log('chiFO', chiFO);
        var chiPx = poissonP(numbers, n, avgChi);
        console.log('chiPx', chiPx);
        var chiFE = chiCuadradoFE(N, chiPx);
        console.log('chiFE', chiFE);
        var prn = exps(avgChi, [12.17, 15.43, 18.7, 21.97, 25.23, 28.5]);
        console.log(prn);
        var maxd = maxim([0.02, 0.16, 0.52, 0.74, 0.92, 0.98, 1], [0.0241, 0.0261, 0.0269, 0.0264, 0.0247, 0.0221, 1]);
        console.log(maxd);
    });
});


function pn(u, v, x) {
    var result = [];
    for (let i = 0; i < x.length; i++) {
        var exp = Math.pow(x[i] - u, 2);
        exp /= (2 * Math.pow(v, 2));
        var px = Math.pow(Math.E, -exp);
        px *= (1 / (v * Math.sqrt(2 * Math.PI)));
        result.push(redondear(px, 4));
    }
    return result;
}

function exps(u, x) {
    var result = [];
    for (let i = 0; i < x.length; i++) {
        var exp = x[i] * u;
        var px = Math.pow(Math.E, -exp);
        px *= u;
        console.log(x[i], px, u)
        result.push(px);
    }
    return result;
}

function maxim(arr1, arr2) {
    var temp = [];
    for (let i = 0; i < arr1.length; i++) {
        temp.push(Math.abs(arr1[i] - arr2[i]))
    }
    return Math.max(...temp);
}